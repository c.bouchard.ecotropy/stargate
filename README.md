# Stargate

A script to quickly move from directories and ssh connection.

## Dependency

Stargate uses `fzf` as its fuzzy finder, and as so, it's required for the installation. You may want to change it for another fuzzy finder, but it requires edition on the stargate script.

## Installation

There is currently no script to automatically install stargate. To install manually copy `stargate.bash` and `stargate_preview` to a known location (in your PATH) and add the following to your bashrc.

```bash
source /path/to/stargate.bash
```
