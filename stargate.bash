#!/usr/bin/env bash

#
# Depends on: fzf.
#

stargate() {
	local STARGATE_DIR="${XDG_CACHE_HOME:-$HOME/.stargate}"
	local GATES_DIR="$STARGATE_DIR/gates"

	mkdir -p "$GATES_DIR"

	add_gate() {
		echo "${2:-$(pwd)}" > "$GATES_DIR/$1"
	}

	alias_gate() {
		ln -s "$GATES_DIR/$1" "$GATES_DIR/$2"
	}
	
	list_gates() {
		/bin/ls "$GATES_DIR" | xargs -n 1
	}

	del_gate() {
		rm -f "$GATES_DIR/$1"
	}
	
	go_gate() {
		if [[ -f "$GATES_DIR/$1" ]]; then
			GATES_PATH=$(cat "$GATES_DIR/$1")
			if [[ -d "$GATES_PATH" ]]; then
				cd "$GATES_PATH"
			else
				ssh "$GATES_PATH"
			fi
		fi
	}

	help_stargate() {
		echo -e "Usage: stargate [COMMAND] [-h,--help]"
		echo -e ""
		echo -e "Commands:"
		echo -e "  add:\t\tAdd a gate."
		echo -e "  alias:\tCreate an alias gate."
		echo -e "  del:\t\tDestroy a gate."
		echo -e "  list:\t\tList known gates."
		echo -e ""
		echo -e "Options:"
		echo -e "  -h, --help:\tShow this help message."
	}
	
	if [[ $# -gt 0 ]]; then
		case "$1" in
			add) shift; add_gate "$1" "$2";;
			list) list_gates;;
			alias) shift; alias_gate "$1" "$2";;
			del) shift; del_gate "$1";;
			--help) help_stargate;;
			-h) help_stargate;;
			*) go_gate $1;;
		esac
	else
		local GATE_PATH=$(/bin/ls "$GATES_DIR" | sort | fzf --layout=reverse --preview="stargate_preview {}" --height=50%)
		go_gate $GATE_PATH
	fi
}
alias sg="stargate"

complete -W "add list alias del" stargate
complete -W "add list alias del" sg
